extern crate num;

use std::ops::Range;
use std::collections::HashMap;

use num::Integer;

#[derive(Debug, Clone, Copy)]
struct Node {
    gen: u64
}

struct Collatz {
    dense: Vec<Option<Node>>,
    sparse: HashMap<u64, Node>,
    cutoff: usize
}

impl Node {
    fn new(gen: u64) -> Node {
        Node {
            gen: gen
        }
    }

    fn one() -> Node {
        Node::new(1)
    }

    fn child(n: Node) -> Node {
       Self::new(n.gen + 1)
    }

    fn calc_parent_val(val: u64) -> u64 {
        let (div, rem) = val.div_rem(&2);
        match rem {
            0 => div,
            _ => val * 3 + 1
        }
    }

    fn calc_even_child_val(val: u64) -> u64 {
        val * 2
    }


    fn calc_odd_child_val(val: u64) -> Option<u64> {
        let (div, rem) = (val - 1).div_rem(&3);
        match rem {
            0 => Some(div),
            _ => None
        }
    }
}

impl Collatz {
    fn new() -> Collatz {
        Self::with_cutoff(10000)
    }

    fn with_cutoff(cutoff: usize) -> Collatz {
        let mut this = Collatz {
            dense: Vec::with_capacity(cutoff),
            sparse: HashMap::new(),
            cutoff: cutoff
        };

        this.dense.push(Some(Node::one()));
        this.dense.push(Some(Node::one()));
        for _ in 2..cutoff {
            this.dense.push(None);
        }

        this
    }

    fn get(&self, x: u64) -> Option<Node> {
        if x < self.cutoff as u64 {
            self.dense[x as usize]
        } else {
            self.sparse.get(&x).map(|n| *n)
        }
    }

    fn put(&mut self, x: u64, n: Node) {
        if x < self.cutoff as u64 {
           self.dense[x as usize] = Some(n);
        } else {
            self.sparse.insert(x,n);
        }
    }

    fn get_or_gen(&mut self, x: u64) -> Node {
        match self.get(x) {
            Some(n) => n,
            None => {
                let p = Node::calc_parent_val(x);
                let n = Node::child(self.get_or_gen(p));
                self.put(p,n);
                n
            }
        }
    }

    fn calc_range(&mut self, r: Range<u64>) -> Vec<u64> {
        let mut result = Vec::new();

        for x in r {
            result.push(self.calc(x));
        }

        result
    }

    fn calc(&mut self, x: u64) -> u64 {
        self.get_or_gen(x).gen
    }

    fn show(&mut self, r: Range<u64>) {
        for x in r {
            println!("{}: {}", x, self.get_or_gen(x).gen);
        }
    }

}



/*
fn nextgen(source: &Vec<u64>) -> Vec<u64> {
    let mut res = Vec::new();
    for x in source {
        let (a, b) = children(*x);
        res.push(a);
        if let Some(v) = b {
            if v != 1 {
                res.push(v)
            }
        }
    }
    res
}

fn nextgen(source: &Vec<u64>) -> Vec<u64> {
    source.iter().flat_map(|x| match children(*x) {
        (a, Some(b)) if b != 1 => vec![a,b],
        (a, _) => vec![a]
    }).collect()
}

fn children(x: u64) -> (u64, Option<u64>) {
    let (div, rem) = (x - 1).div_rem(&3);
    (
        x * 2,
        match rem {
            0 if div > 0 => Some(div),
            _ => None
        }
    )
}

fn gen_to(generations: usize) -> Vec<Vec<u64>> {
    let mut result = Vec::with_capacity(generations);

    result.push(vec![1]);
    for i in 0..(generations-1) {
        let next_gen = nextgen(&result[i]);
        result.push(next_gen);
    }
    result
}

fn num_of(x: u64) -> usize {
    let result = 0;
    while x > 1 {
        if x & 1 == 0 {
            x = x >> 1;
        } else {
            x = x * 3 + 1;
        }
        result++;
    }
    result
}

fn backward<T>(size: T) -> Vec<Node<T>> {
    let mut result: Vec<Option<Node<T>>> = Vec::with_capacity(size);

    result[0] = Node<T>::zero();

    for i in 2..size {




    result.iter().map(|o| o.unwrap()).collect()
}
*/

fn main() {
    let mut cz = Collatz::new();

    cz.show(1..100);

}

